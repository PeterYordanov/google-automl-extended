﻿namespace GoogleAutoMLExtended.Enumerations
{
    public enum ModelType
    {
        ObjectDetection,
        ImageClassification,
        TextSentiment,
        TextExtraction
    }
}
