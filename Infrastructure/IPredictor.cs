﻿using System.IO;
using GoogleAutoMLExtended.Models;

namespace GoogleAutoMLExtended.Infrastructure
{
    interface IPredictor
    {
        public PredictionResults Predict(byte[] data, string scoreThreshold = "0.5");
        public PredictionResults Predict(MemoryStream stream);
    }
}
