﻿using Google.Cloud.AutoML.V1;
using System.Threading.Tasks;
using GoogleAutoMLExtended.Models;
using GoogleAutoMLExtended.Enumerations;

namespace GoogleAutoMLExtended.Infrastructure
{
    public class ModelDeployer
    {
        private readonly AutoMLAuthenticationParameters authenticationParameters;
        private readonly string modelFullId;
        private readonly AutoMlClient client;

        public ModelDeployer(AutoMLAuthenticationParameters parameters)
        {
            authenticationParameters = parameters;

            modelFullId = ModelName.Format(authenticationParameters.ProjectId, 
                                           parameters.LocationId, 
                                           authenticationParameters.ModelId);

            AutoMlClientBuilder builder = new AutoMlClientBuilder
            {
                CredentialsPath = parameters.KeyPath
            };

            client = builder.Build();
        }

        public async Task Deploy(ModelType modelType)
        {
            long nodeCount = authenticationParameters.NodeCount ?? 1;

            switch (modelType)
            {
                case ModelType.ObjectDetection:
                    ImageObjectDetectionModelDeploymentMetadata metadataObjectDetection =
                    new ImageObjectDetectionModelDeploymentMetadata
                    {
                        NodeCount = nodeCount
                    };

                    DeployModelRequest requestObjectDetection = new DeployModelRequest
                    {
                        Name = modelFullId,
                        ImageObjectDetectionModelDeploymentMetadata = metadataObjectDetection
                    };

                    var resultObjectDetection = await client.DeployModelAsync(requestObjectDetection);
                    await resultObjectDetection.PollUntilCompletedAsync();
                    break;

                case ModelType.ImageClassification:
                    ImageClassificationModelDeploymentMetadata metadataImageClassification =
                    new ImageClassificationModelDeploymentMetadata
                    {
                        NodeCount = nodeCount
                    };

                    DeployModelRequest request = new DeployModelRequest
                    {
                        Name = modelFullId,
                        ImageClassificationModelDeploymentMetadata = metadataImageClassification
                    };

                    var resultImageClassification = await client.DeployModelAsync(request);
                    await resultImageClassification.PollUntilCompletedAsync();
                    break;
                case ModelType.TextSentiment:
                    break;
                case ModelType.TextExtraction:
                    break;
                default:
                    throw new System.Exception("No such model type available for deployment");
            }
        }

        public async Task Undeploy()
        {
            UndeployModelRequest request = new UndeployModelRequest
            {
                Name = modelFullId
            };

            var result = await client.UndeployModelAsync(request);
            await result.PollUntilCompletedAsync();
        }

        public async Task<ModelMetadata> GetModelMetadata()
        {
            Model model = await client.GetModelAsync(new GetModelRequest { Name = modelFullId });
            ModelName modelName = model.ModelName;

            return new ModelMetadata
            {
                DeploymentState = model.DeploymentState,
                DatasetID = model.DatasetId,
                DisplayName = model.DisplayName,
                ModelName = model.Name,
                ProjectID = modelName.ProjectId,
                LocationID = modelName.LocationId
            };
        }

        public async Task<bool> IsDeployed()
        {
            ModelMetadata metadata = await GetModelMetadata();
            return metadata.DeploymentState == Model.Types.DeploymentState.Deployed;
        }

        public async Task<bool> IsUndeployed()
        {
            ModelMetadata metadata = await GetModelMetadata();
            return metadata.DeploymentState == Model.Types.DeploymentState.Undeployed;
        }

        public async Task<bool> IsUnspecified()
        {
            ModelMetadata metadata = await GetModelMetadata();
            return metadata.DeploymentState == Model.Types.DeploymentState.Unspecified;
        }
    }
}
