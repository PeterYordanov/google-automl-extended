﻿using Google.Apis.Auth.OAuth2;
using Google.Cloud.AutoML.V1;
using Google.Protobuf;
using Grpc.Auth;
using ImageMagick;
using System.Collections.Generic;
using System.IO;
using System.Net;
using GoogleAutoMLExtended.Models;

namespace GoogleAutoMLExtended.Infrastructure
{
    public class ObjectDetector : IPredictor
    {
        private readonly AutoMLAuthenticationParameters authenticationParameters;
        private readonly string modelFullId;
        private readonly PredictionServiceClient client;

        public ObjectDetector(AutoMLAuthenticationParameters parameters)
        {
            authenticationParameters = parameters;
            modelFullId = ModelName.Format(authenticationParameters.ProjectId, parameters.LocationId, authenticationParameters.ModelId);

            GoogleCredential credential = GoogleCredential.FromFile(authenticationParameters.KeyPath)
                                             .CreateScoped(PredictionServiceClient.DefaultScopes);

            WebRequest.DefaultWebProxy.Credentials = CredentialCache.DefaultNetworkCredentials;

            PredictionServiceClientBuilder predictionServiceClientBuilder = new PredictionServiceClientBuilder
            {
                ChannelCredentials = credential.ToChannelCredentials()
            };

            client = predictionServiceClientBuilder.Build();
        }

        public PredictionResults Predict(MemoryStream stream)
        {
            return Predict(stream.ToArray());
        }

        public PredictionResults Predict(byte[] data, string scoreThreshold = "0.5")
        {
            ByteString content = ByteString.CopyFrom(data);

            Image image = new Image
            {
                ImageBytes = content
            };

            ExamplePayload payload = new ExamplePayload
            {
                Image = image
            };

            PredictRequest predictRequest = new PredictRequest
            {
                Name = modelFullId,
                Payload = payload,
                Params =
                {
                    { "score_threshold", scoreThreshold }
                }
            };

            PredictResponse response = client.Predict(predictRequest);

            PredictionResults predictionResults = new PredictionResults
            {
                Results = new List<PredictionResult>()
            };

            MagickImage imageMagick = new MagickImage(data);
            predictionResults.OriginalImage = new MagickImage(data);

            foreach (AnnotationPayload annotationPayload in response.Payload)
            {
                var vertices = annotationPayload.ImageObjectDetection.BoundingBox.NormalizedVertices;

                NormalizedVertex upperBounds = vertices[0];
                NormalizedVertex lowerBounds = vertices[1];

                float x = upperBounds.X, x1 = lowerBounds.X;
                float y = upperBounds.Y, y1 = lowerBounds.Y;

                DrawableStrokeColor strokeColor = new DrawableStrokeColor(ColorConstants.LightGreen);
                DrawableStrokeWidth stokeWidth = new DrawableStrokeWidth(3);
                DrawableFillColor fillColor = new DrawableFillColor(MagickColors.Transparent);

                DrawableRectangle circle = new DrawableRectangle(x * imageMagick.Width, 
                                                                 y * imageMagick.Height, 
                                                                 x1 * imageMagick.Width, 
                                                                 y1 * imageMagick.Height);

                imageMagick.Draw(strokeColor, stokeWidth, fillColor, circle);

				DrawableText drawableText = new DrawableText(x * imageMagick.Width + 5, 
                                                             y * imageMagick.Height + 20, 
                                                             $" Label: { annotationPayload.DisplayName }\n Score: { annotationPayload.ImageObjectDetection.Score }");

                DrawableStrokeColor strokeColorText = new DrawableStrokeColor(ColorConstants.Black);
                imageMagick.Draw(strokeColorText, drawableText);

                PredictionResult result = new PredictionResult
                {
                    Label = annotationPayload.DisplayName,
                    Score = annotationPayload.ImageObjectDetection.Score
                };

                predictionResults.Results.Add(result);
            }

            predictionResults.Image = imageMagick;
            predictionResults.ObjectsDetected = predictionResults.Results.Count;

            return predictionResults;
        }
    }
}
