﻿using Google.Apis.Auth.OAuth2;
using Google.Cloud.AutoML.V1;
using Google.Protobuf;
using Grpc.Auth;
using ImageMagick;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using GoogleAutoMLExtended.Models;

namespace GoogleAutoMLExtended.Infrastructure
{
    public class TextExtraction : IPredictor
    {
        private readonly AutoMLAuthenticationParameters authenticationParameters;
        private readonly string modelFullId;
        private readonly PredictionServiceClient client;

        public TextExtraction(AutoMLAuthenticationParameters parameters)
        {
            authenticationParameters = parameters;
            modelFullId = ModelName.Format(authenticationParameters.ProjectId, parameters.LocationId, authenticationParameters.ModelId);

            GoogleCredential credential = GoogleCredential.FromFile(authenticationParameters.KeyPath)
                                             .CreateScoped(PredictionServiceClient.DefaultScopes);

            WebRequest.DefaultWebProxy.Credentials = CredentialCache.DefaultNetworkCredentials;

            PredictionServiceClientBuilder predictionServiceClientBuilder = new PredictionServiceClientBuilder
            {
                ChannelCredentials = credential.ToChannelCredentials()
            };

            client = predictionServiceClientBuilder.Build();
        }

        public PredictionResults Predict(MemoryStream stream)
        {
            return Predict(stream.ToArray());
        }

        public PredictionResults Predict(byte[] data, string scoreThreshold = "0.5")
        {
            ByteString content = ByteString.CopyFrom(data);

            Image image = new Image
            {
                ImageBytes = content
            };

            ExamplePayload payload = new ExamplePayload
            {
                Image = image
            };

            PredictRequest predictRequest = new PredictRequest
            {
                Name = modelFullId,
                Payload = payload,
                Params =
                {
                    { "score_threshold", scoreThreshold }
                }
            };

            PredictResponse response = client.Predict(predictRequest);

            PredictionResults predictionResults = new PredictionResults
            {
                Results = new List<PredictionResult>()
            };

            MagickImage imageMagick = new MagickImage(data);
            predictionResults.OriginalImage = new MagickImage(data);

            foreach (AnnotationPayload annotationPayload in response.Payload)
            {
                PredictionResult result = new PredictionResult
                {
                    Label = annotationPayload.DisplayName,
                    Segment = annotationPayload.TextExtraction.TextSegment.Content,
                    Score = annotationPayload.TextExtraction.Score
                };

                predictionResults.Results.Add(result);
            }

            predictionResults.Image = imageMagick;
            predictionResults.ObjectsDetected = predictionResults.Results.Count;

            return predictionResults;
        }
    }
}
