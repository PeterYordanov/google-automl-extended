﻿namespace GoogleAutoMLExtended.Models
{
    public class AutoMLAuthenticationParameters
    {
        public string ProjectId { get; set; }

        public string ModelId { get; set; }

        public string KeyPath { get; set; }

        public string LocationId { get; set; }

        public int? NodeCount { get; set; }
    }
}
