﻿using ImageMagick;

namespace GoogleAutoMLExtended.Models
{
    public static class ColorConstants
    {
        public static MagickColor LightGreen = MagickColor.FromRgb(0, 230, 0);

        public static MagickColor Black = MagickColor.FromRgb(0, 0, 0);

        public static MagickColor White = MagickColor.FromRgb(255, 255, 255);
    }
}
