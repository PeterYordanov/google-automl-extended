﻿using Google.Cloud.AutoML.V1;

namespace GoogleAutoMLExtended.Models
{
    public class ModelMetadata
    {
        public string DatasetID { get; set; }
        
        public string ProjectID { get; set; }
        
        public string LocationID { get; set; }
        
        public string ModelName { get; set; }
        
        public string DisplayName { get; set; }

        public Model.Types.DeploymentState DeploymentState { get; set; }
    }
}
