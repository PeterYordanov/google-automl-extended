﻿namespace GoogleAutoMLExtended.Models
{
    public class PredictionResult
    {
        public float Score { get; set; }

        public string Label { get; set; }

        public string Translation { get; set; }
    
        public int Sentiment { get; set; }

        public string Segment { get; set; }
    }
}
