﻿using ImageMagick;
using System.Collections.Generic;

namespace GoogleAutoMLExtended.Models
{
    public class PredictionResults
    {
        public List<PredictionResult> Results { get; set; }

        public MagickImage Image { get; set; }

        public MagickImage OriginalImage { get; set; }

        public int ObjectsDetected { get; set; }
    }
}
